# frozen_string_literal: true
require_relative '../spec_helper'

RSpec.describe Renums::Enum do

  let(:array_def) { %i(Foo Bar Bim) }
  let(:default_enum_for_3_keys) { (0..2).to_a }
  let(:fixed_enum_values) { [1, 5, 10] }
  let(:hash_def) { Hash[array_def.zip(fixed_enum_values)] }

  subject { described_class }

  it 'should be possible to create an enum from an array' do
    expect do
      s = subject.new array_def
      expect(s.enum_names).to eq array_def
    end .not_to raise_error
  end

  it 'should be possible to create an enum from a dereferenced array' do
    expect do
      s = subject.new(*array_def)
      expect(s.enum_names).to eq array_def
    end .not_to raise_error
  end

  it 'should be possible to create an enum from a hash' do
    expect do
      s = subject.new hash_def
      expect(s.enum_names).to eq array_def
      expect(s.enum_values).to eq fixed_enum_values
    end .not_to raise_error
  end

  context 'when created from an array' do
    subject { described_class.new array_def }


    it 'all enum values should be consecutive, starting with 0' do
      expect(subject.enum_values).to eq default_enum_for_3_keys
    end

    it 'enums names should be returned in the same order they were created' do
        expect(subject.enum_names).to eq array_def
    end
  end

  context 'when created from an hash' do
    subject { described_class.new hash_def }

    it 'enums names should be returned in the same order they were created' do
      expect(subject.enum_names).to eq array_def
    end

    it 'all enum values should be exactly the one specified in the hash' do
      expect(subject.enum_values).to eq fixed_enum_values
    end

  end

  context 'when declaring an enum' do
    subject { described_class.new hash_def }

    it 'should respond to any enum value declared as a method' do
      hash_def.keys.each do |enum_name|
        expect(subject).to respond_to enum_name
        expect(subject.send enum_name).to eq hash_def[enum_name]
      end
    end

    it 'should be possible to iterate over the enum' do
      expect(subject.map &:to_s).to eq fixed_enum_values.map(&:to_s)
    end

  end


end
