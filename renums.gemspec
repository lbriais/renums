# frozen_string_literal: true

require_relative 'lib/renums/version'

Gem::Specification.new do |spec|
  spec.name          = 'renums'
  spec.version       = Renums::VERSION
  spec.authors       = ['Laurent B.']
  spec.email         = ['lbnetid+rb@gmail.com']

  spec.summary       = 'Simple Enums implementation'
  spec.description   = 'Easy to use enums in your Ruby code'
  spec.license       = 'MIT'
  spec.homepage      = 'https://gitlab.com/lbriais/renums'

  spec.metadata['allowed_push_host'] = 'https://gems.nanonet'

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = '%s/%s' % [spec.metadata["source_code_uri"], 'CHANGELOG.md' ]

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'pry'

end
