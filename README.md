# Renums

This Gem provides simple mechanism to provide Enum-like features to Ruby objects and classes

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'renums'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install renums

## Usage

```ruby
require 'renums'

# Either you directly use the Renums::Enum class
my_enum = Renums::Enum.new :state_0, :state_1, :state_2

# alternatively, you can include the Renums module and use the helper method
include Renums
my_enum = define_enum :state_0, :state_1, :state_2

# To use in your class definitions
# either you extend the module in your class
class MyClass
  extend Renums
  STATES =  define_enum :state_0, :state_1, :state_2
end

# Or you can use the Renums helper
# To be able to use in any class
Renums.inject_in_all_objects
# or to add it to a specific class and descendants
Renums.inject_in_classes_tree MyClass

# Then you can directly do
class MyClass  
  STATES =  define_enum :state_0, :state_1, :state_2
end

# Once you have a variable/constant declared as an Enum, you can directly access enums value using methods

my_states = define_enum :state_0, :state_1, :state_2
my_states.state_1 # => returns 1

# Enum instances are Enumerable, so you can use any of the usual methods
my_states.each {|s| ... } # where s is the value of each "state", ie 0, 1, 2

# Two methods are also provided
my_states.enum_names # => returns [:state_0, :state_1, :state_2]
my_states.enum_values # => returns [0, 1, 2]

# You can create enums of non consecutive values using an initializer hash
my_http_states = define_enum({HTTP_OK: 200, HTTP_SERVER_ERROR: 500})
my_http_states.enum_names # => returns [:HTTP_OK, :HTTP_SERVER_ERROR]
my_http_states.enum_values # => returns [200, 500]
my_http_states.HTTP_SERVER_ERROR # => returns 500

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/lbriais/renums. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gilab.com/lbriais/renums/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Renums project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gilab.com/lbriais/renums/blob/master/CODE_OF_CONDUCT.md).
