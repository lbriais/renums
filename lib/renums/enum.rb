module Renums
  class Enum

    include Enumerable

    def initialize(*args)
      hash = case args.first
             when Hash
               args.first
             when Array
               Hash[args.first.zip(args.first.map.with_index {|_, index| index } )]
             else
               Hash[args.zip(args.map.with_index {|_, index| index } )]
             end
      @enum_hash = process_hash(hash).freeze
    end

    def each(&block)
      enum_hash.values.sort.each &block
    end

    def enum_values
      each.to_a
    end

    def enum_names
      enum_hash.keys.sort { |a, b| enum_hash[a] <=> enum_hash[b] }
    end

    def to_hash
      enum_hash.dup
    end

    def respond_to?(method_name, include_private = false)
      enum_hash.keys.include?(method_name) || super
    end

    def method_missing(method_name, *args, &block)
      if enum_hash.keys.include? method_name
        return enum_hash[method_name]
      end
      super(method_name, *args, &block)
    end


    private

    attr_reader :enum_hash

    def process_hash(hash)
      keys = hash.keys.map do |k|
        case k
        when Symbol
          k
        when String
          k.to_symbol
        else
          raise Renums::Error, "Invalid enum identifier '#{k.inspect}'"
        end
      end
      values = hash.values.map do |v|
        raise Renums::Error, "Invalid value for enum '#{v.inspect}'" unless v.is_a? Integer
        v
      end
      Hash[keys.zip(values)]
    end

  end
end
