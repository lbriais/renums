# frozen_string_literal: true

require_relative 'renums/version'
require_relative 'renums/enum'

module Renums

  class Error < StandardError; end

  def define_enum(*args)
    Renums::Enum.new *args
  end

  def self.inject_in_classes_tree(default=Object)
    default.class_eval do
      extend Renums
    end
  end

  def self.inject_in_all_objects
    inject_in_classes_tree
  end

end
